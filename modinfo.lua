name = "Relaxed Crafting"
description = "Pauses the game while the crafting menu is open or a placeable item is selected."
author = "noobler\n(SW fix by Kam297)"
version = "1.3"
forumthread = "/files/file/340-relaxed-crafting/"
api_version = 6
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 2.5
dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true
hamlet_compatible = true

configuration_options =
{
    {
        name = "ct",
		label= "Hold CTRL Toggle to stop pausing",
        options =
        {
            {description = "Enabled", data = true},
            {description = "Disabled", data = false},
        },
        default = true,
    },
    {
        name = "ctplacement",
		label= "Placemnent pause CTRL toggle",
        options =
        {
			{description = "Always paused", data = "always"},
            {description = "Normal", data = "normal"},
            {description = "Inverted", data = "inverted"},
			{description = "Never pause", data = "disabled"},
        },
        default = "always",
    }
}