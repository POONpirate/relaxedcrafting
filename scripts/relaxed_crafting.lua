local player = GetPlayer()
local modname = "Relaxed Crafting"
local mod = KnownModIndex:GetModActualName(modname)

if player.components.builder.relaxedcrafting_OriginalMakeRecipe == nil then
	player.components.builder.relaxedcrafting_OriginalMakeRecipe = player.components.builder.MakeRecipe
else
	print("relaxedcrafting_OriginalMakeRecipe already set")
end

if player.components.inventory.relaxedcrafting_OriginalSetActiveItem == nil then
	player.components.inventory.relaxedcrafting_OriginalSetActiveItem = player.components.inventory.SetActiveItem
else
	print("relaxedcrafting_OriginalSetActiveItem already set")
end

if player.components.playercontroller.relaxedcrafting_OriginalCancelPlacement == nil then
	player.components.playercontroller.relaxedcrafting_OriginalCancelPlacement = player.components.playercontroller.CancelPlacement
else
	print("relaxedcrafting_OriginalCancelPlacement already set")
end

if player.components.playercontroller.relaxedcrafting_OriginalOnControl == nil then
	player.components.playercontroller.relaxedcrafting_OriginalOnControl = player.components.playercontroller.OnControl
else
	print("relaxedcrafting_OriginalOnControl already set")
end

if player.components.playercontroller.relaxedcrafting_OriginalOnUpdate == nil then
	player.components.playercontroller.relaxedcrafting_OriginalOnUpdate = player.components.playercontroller.OnUpdate
else
	print("relaxedcrafting_OriginalOnUpdate already set")
end

-- attached to builder.MakeRecipe
local function MakeRecipe(builder, recipe, pt, onsuccess, made)
	if made then
		SetPause(false)
		--print("unpause - made recipe")
	end
end

-- attached to inventory.SetActiveItem
local function SetActiveItem(inventory, item)
	if not IsPaused() or (TheFrontEnd:GetActiveScreen() and (TheFrontEnd:GetActiveScreen().name == "TMIScreen" or TheFrontEnd:GetActiveScreen().name == "LoginScreen" or TheFrontEnd:GetActiveScreen().name == "SignScreen" or TheFrontEnd:GetActiveScreen().name == "ModConfig" or TheFrontEnd:GetActiveScreen().name == "FA_CharRenameScreen" or TheFrontEnd:GetActiveScreen().name == "FARecipeBookScreen" or TheFrontEnd:GetActiveScreen().name == "FASpellBookScreen" or TheFrontEnd:GetActiveScreen().name == "htp")) then return end
	if not item or (item and not item.components.deployable) then
		SetPause(false)
		--print("unpause - no active item")
	end
end

-- attached to playercontroller.OnUpdate
local function OnUpdate(playercontroller, dt)
	if not IsPaused() then
		if playercontroller.inst.sg:HasStateTag("idle") and not playercontroller.inst.components.locomotor.bufferedaction then
			if (playercontroller.placer and playercontroller.placer_recipe) or (playercontroller.deployplacer and playercontroller.deployplacer.components.placer) then
				if GetModConfigData("ctplacement", mod) ~= "disabled" then
					if GetModConfigData("ctplacement", mod) == "normal" and not TheInput:IsKeyDown(KEY_CTRL) then
						SetPause(true)
					elseif GetModConfigData("ctplacement", mod) == "inverted" and TheInput:IsKeyDown(KEY_CTRL) then
						SetPause(true)
					elseif GetModConfigData("ctplacement", mod) == "always" then
						SetPause(true)
					end
				end
			end
		end
	else
		if playercontroller.deployplacer and playercontroller.deployplacer.components.placer then
			playercontroller.LMBaction, playercontroller.RMBaction = playercontroller.inst.components.playeractionpicker:DoGetMouseActions()
		end
	end
end

-- attached to playercontroller.CancelPlacement
local function CancelPlacement(playercontroller)
	if not IsPaused() then return end
	SetPause(false)
	--print("unpause - cancel placement")
end

-- attached to playercontroller.OnControl
local function OnControl(playercontroller, control, down)
	if not playercontroller:IsEnabled() then return end
	
	-- fix by squeek
	-- if GetWorld().minimap.MiniMap:IsVisible() then return end
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen():is_a( require "screens/mapscreen" ) then return end
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name == "ConsoleScreen" then return end
	-- fix by squeek
	
	if not IsPaused() then return end
	if not down then return end
	-- if not (playercontroller.placer_recipe or (playercontroller.deployplacer and playercontroller.deployplacer.components.placer)) then return end
	if control == CONTROL_CANCEL then
		playercontroller:CancelPlacement()
	elseif control == CONTROL_PRIMARY then
		playercontroller:OnLeftClick(down)
	elseif control == CONTROL_SECONDARY then
		playercontroller:OnRightClick(down)
	elseif control == CONTROL_ACTION then
		playercontroller:DoActionButton()
	elseif control == CONTROL_ROTATE_LEFT then
		TheCamera:SetHeadingTarget(TheCamera:GetHeadingTarget() - 45)
		TheCamera:Update(1)
	elseif control == CONTROL_ROTATE_RIGHT then
		TheCamera:SetHeadingTarget(TheCamera:GetHeadingTarget() + 45)
		TheCamera:Update(1)
	elseif control == CONTROL_ZOOM_IN then
		TheCamera:ZoomIn()
		TheCamera:Update(1)
	elseif control == CONTROL_ZOOM_OUT then
		TheCamera:ZoomOut()
		TheCamera:Update(1)
	end
	if (control == CONTROL_MOVE_UP or control == CONTROL_MOVE_DOWN or control == CONTROL_MOVE_LEFT or control == CONTROL_MOVE_RIGHT)  and not (TheFrontEnd:GetActiveScreen() and (TheFrontEnd:GetActiveScreen().name == "TMIScreen" or TheFrontEnd:GetActiveScreen().name == "LoginScreen" or TheFrontEnd:GetActiveScreen().name == "SignScreen" or TheFrontEnd:GetActiveScreen().name == "ModConfig" or TheFrontEnd:GetActiveScreen().name == "FA_CharRenameScreen" or TheFrontEnd:GetActiveScreen().name == "FARecipeBookScreen" or TheFrontEnd:GetActiveScreen().name == "FASpellBookScreen" or TheFrontEnd:GetActiveScreen().name == "htp")) then
		SetPause(false)
		playercontroller.inst.sg:RemoveStateTag("idle")
		--print("unpause - direct walking")
	elseif playercontroller.inst.components.locomotor.bufferedaction and not (TheFrontEnd:GetActiveScreen() and (TheFrontEnd:GetActiveScreen().name == "TMIScreen" or TheFrontEnd:GetActiveScreen().name == "LoginScreen" or TheFrontEnd:GetActiveScreen().name == "SignScreen" or TheFrontEnd:GetActiveScreen().name == "ModConfig" or TheFrontEnd:GetActiveScreen().name == "FA_CharRenameScreen" or TheFrontEnd:GetActiveScreen().name == "FARecipeBookScreen" or TheFrontEnd:GetActiveScreen().name == "FASpellBookScreen" or TheFrontEnd:GetActiveScreen().name == "htp")) then
		-- upause to drop or place deployable
		SetPause(false)
		--print("unpause - action on control")
	end
end

-- overwrite builder.MakeRecipe
if IsDLCEnabled(CAPY_DLC) or IsDLCEnabled(PORKLAND_DLC) then
	player.components.builder.MakeRecipe = function(self, recipe, pt, rot, onsuccess)
			local made = self:relaxedcrafting_OriginalMakeRecipe(recipe, pt, rot, onsuccess)
			local successflag, retvalue = pcall(MakeRecipe, self, recipe, pt, rot, onsuccess, made)
			if not successflag then
				print(retvalue)
			end
		end
else
	player.components.builder.MakeRecipe = function(self, recipe, pt, onsuccess)
			local made = self:relaxedcrafting_OriginalMakeRecipe(recipe, pt, onsuccess)
			local successflag, retvalue = pcall(MakeRecipe, self, recipe, pt, onsuccess, made)
			if not successflag then
				print(retvalue)
			end
		end
end
	
-- overwrite inventory.SetActiveItem
player.components.inventory.SetActiveItem = function(self, item)
		self:relaxedcrafting_OriginalSetActiveItem(item)
		local successflag, retvalue = pcall(SetActiveItem, self, item)
		if not successflag then
			print(retvalue)
		end
	end
	
-- overwrite playercontroller.CancelPlacement
player.components.playercontroller.CancelPlacement = function(self)
		self:relaxedcrafting_OriginalCancelPlacement()
		local successflag, retvalue = pcall(CancelPlacement, self)
		if not successflag then
			print(retvalue)
		end
	end

-- overwrite playercontroller.OnControl
player.components.playercontroller.OnControl = function(self, control, down)
		self:relaxedcrafting_OriginalOnControl(control, down)
		local successflag, retvalue = pcall(OnControl, self, control, down)
		if not successflag then
			print(retvalue)
		end
	end
	
-- overwrite playercontroller.OnUpdate
player.components.playercontroller.OnUpdate = function(self, dt)
		self:relaxedcrafting_OriginalOnUpdate(dt)
		local successflag, retvalue = pcall(OnUpdate, self, dt)
		if not successflag then
			print(retvalue)
		end
	end
	
print("script loaded")