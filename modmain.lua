local function LoadScript(filename)
	local fn = GLOBAL.loadfile("scripts/"..filename..".lua")
	if not fn then return end
	local successflag, retvalue = GLOBAL.pcall(fn)
	if successflag then
		GLOBAL.GetPlayer().SoundEmitter:PlaySound("dontstarve/HUD/collect_newitem")
	else
		print(retvalue)
	end
end

local function OnSimPostInit(player)
	LoadScript("relaxed_crafting")
	GLOBAL.TheInput:AddKeyDownHandler(GLOBAL.KEY_KP_DIVIDE, function()
			LoadScript("relaxed_crafting")
		end)
	GLOBAL.TheInput:AddMoveHandler(function()
			if not GLOBAL.IsPaused() then return end
			local player = GLOBAL.GetPlayer()
			if not player then return end
			if not player.components.playercontroller then return end

			-- update placer if exists
			if player.components.playercontroller.placer then
				if player.components.playercontroller.placer.components.placer then
					player.components.playercontroller.placer.components.placer:OnUpdate(0)
				end
			end
			
			-- update deployplacer if exists
			if player.components.playercontroller.deployplacer then
				if player.components.playercontroller.deployplacer.components.placer then
					player.components.playercontroller.deployplacer.components.placer:OnUpdate(0)
					player.components.playercontroller:OnUpdate(0)
				end
			end
		end)
end

AddSimPostInit(OnSimPostInit)

local function RelaxedCrafting(self)
	local OriginalClose = self.Close
	local OriginalScrollDown = self.ScrollDown
	local OriginalScrollUp = self.ScrollUp
	
	function self:Close(fn)
		OriginalClose(self, fn)
		if not self.owner.components.playercontroller.placer then
			GLOBAL.SetPause(false)
		end
	end

	function self:ScrollDown()
		GLOBAL.SetPause(false)
		OriginalScrollDown(self)
		if GetModConfigData("ct") and GLOBAL.TheInput:IsKeyDown(GLOBAL.KEY_CTRL) then return end
		GLOBAL.SetPause(true)
	end
	
	function self:ScrollUp()
		GLOBAL.SetPause(false)
		OriginalScrollUp(self)
		if GetModConfigData("ct") and GLOBAL.TheInput:IsKeyDown(GLOBAL.KEY_CTRL) then return end
		GLOBAL.SetPause(true)
	end
end

AddClassPostConstruct("widgets/crafting", RelaxedCrafting)

local function OnCraftTabsPostConstruct(self)
	local OriginalOnUpdate = self.OnUpdate
	
	function self:OnUpdate(dt)
		OriginalOnUpdate(self, dt)
		if not (self.crafting.open or self.controllercraftingopen) then return end
		if GLOBAL.IsPaused() then return end
		if GLOBAL.GetPlayer().components.locomotor.bufferedaction then return end
		if not GLOBAL.GetPlayer().components.playercontroller.inst.sg:HasStateTag("idle") then return end
		if GetModConfigData("ct") and GLOBAL.TheInput:IsKeyDown(GLOBAL.KEY_CTRL) then return end
		GLOBAL.SetPause(true)
	end
end
AddClassPostConstruct("widgets/crafttabs", OnCraftTabsPostConstruct)

local function CameraPostConstruct(self)
	function self:ZoomIn()
	if GLOBAL.GetPlayer().HUD:IsCraftingOpen() then return end --self.crafting.open self.controllercraftingopen
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name == "PauseScreen" then return end
    self.distancetarget = self.distancetarget - self.zoomstep
    if self.distancetarget < self.mindist then
        self.distancetarget = self.mindist
    end
    self.time_since_zoom = 0
	end
	
	function self:ZoomOut()
	if GLOBAL.GetPlayer().HUD:IsCraftingOpen() then return end
	if TheFrontEnd:GetActiveScreen() and TheFrontEnd:GetActiveScreen().name == "PauseScreen" then return end
    self.distancetarget = self.distancetarget + self.zoomstep
    if self.distancetarget > self.maxdist then
        self.distancetarget = self.maxdist
    end
	self.time_since_zoom = 0
	end
	
end

AddClassPostConstruct("cameras/followcamera", CameraPostConstruct)